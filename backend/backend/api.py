import logging
import backend.docutils
import os, json, subprocess

from multiprocessing import Pool
from sqlalchemy.exc import IntegrityError
from flask import Response, jsonify, request, redirect
from werkzeug.utils import secure_filename
from backend.db_info import app, db, documents_schema, Document, Category, SubCategory, \
        Model, ModelType, Frequency, OrderOfBattle, DocumentSchema

gunicorn_logger = logging.getLogger('gunicorn.error')
app.logger.handlers = gunicorn_logger.handlers
app.logger.setLevel(gunicorn_logger.level)

def find_match(mt_name, text):
    return (mt_name, backend.docutils.found_pattern(mt_name, text))

# update db with added/updated docs
@app.route('/api/process', methods=['GET'])
def process():
    pool = Pool()
    subprocess.run("recevent.sh")
    data = backend.docutils.process()
    model_types = [ modtype.name for modtype in ModelType.query.all() ]
    for docpath, text in data:
        doc = Document(os.path.basename(docpath), docpath)
        db.session.add(doc)
        try: db.session.commit()
        except IntegrityError:
            db.session.rollback() # we have an update, redo frequencies
            doc = Document.query.filter_by(file_path=docpath).first()
            db.session.query(Frequency).filter(Frequency.doc_id == doc.id).delete()
        args = [(mt_name, text) for mt_name in model_types]
        matches = pool.starmap(find_match, args)
        for mt_name, match in matches:
            if match:
                modtype = ModelType.query.filter_by(name=mt_name).first()
                ob = OrderOfBattle.query.filter_by(modtype_id=modtype.id).first()
                db.session.add(Frequency(doc.id, ob.cat_id, ob.subcat_id, ob.mod_id, ob.modtype_id))
                try: db.session.commit()
                except: db.session.rollback()
    return jsonify(success=True), 200

# clean db of removed docs
@app.route('/api/clean', methods=['GET'])
def clean():
    subprocess.run("recevent.sh")
    removed = backend.docutils.clean()
    for path in removed:
        doc = Document.query.filter_by(file_path=path).first()
        # bulk delete frequencies associated with this document
        db.session.query(Frequency).filter(Frequency.doc_id == doc.id).delete()
        db.session.delete(doc)
    db.session.commit()
    return jsonify(success=True), 200

# construct and return json object
@app.route('/api/data/sunburst', methods=['GET'])
def sunburst():
    with open("demo.json", "r") as json_file:
        data = json.load(json_file)
    return jsonify(data)

# return current list of documents in db
@app.route('/api/data/docs', methods=['GET'])
def docs():
    all_docs = Document.query.all()
    return documents_schema.jsonify(all_docs)

# return list of frequencies for flare model
@app.route('/api/data/flare', methods=['GET'])
def frequencies():
    response = {
            "name": "OrderOfBattle",
            "children": []
            }

    for distinct_category in db.session.query(OrderOfBattle.cat_id).distinct():
        cat_id = distinct_category.cat_id
        # add category to response
        cat_name = db.session.query(Category).filter(Category.id == cat_id).first().name
        cat_dict = {
                "name": cat_name,
                "children": []
                }
        # add subcategories, if they exist
        for distinct_subcat in db.session.query(OrderOfBattle.subcat_id).filter(OrderOfBattle.cat_id == cat_id).distinct():
            # only add subcategories that belong to the current category
            subcat_id = distinct_subcat.subcat_id
            subcat_name = db.session.query(SubCategory).filter(SubCategory.id == subcat_id).first().name
            subcat_dict = {"name": subcat_name, "children": []}
            # add models, if they exist
            for distinct_model in db.session.query(OrderOfBattle.mod_id).filter(OrderOfBattle.cat_id == cat_id, OrderOfBattle.subcat_id == subcat_id).distinct():
                mod_id = distinct_model.mod_id
                mod_name = db.session.query(Model).filter(Model.id == mod_id).first().name
                mod_dict = {"name": mod_name, "children": []}
                # add modeltypes and frequencies
                for distinct_modeltype in db.session.query(OrderOfBattle.modtype_id).filter(OrderOfBattle.cat_id == cat_id, OrderOfBattle.subcat_id == subcat_id, OrderOfBattle.mod_id == mod_id).distinct():
                    modtype_id = distinct_modeltype.modtype_id
                    modtype_name = db.session.query(ModelType).filter(ModelType.id == modtype_id).first().name
                    size_query = """select count(Frequency.modtype_id) as 'size' from ModelType left join Frequency on ModelType.id = Frequency.modtype_id where Frequency.modtype_id = """ + str(modtype_id)
                    modtype_size = db.session.execute(size_query).first()
                    modtype_dict = {"name": modtype_name, "size": modtype_size[0]}
                    prev_mod_children = mod_dict["children"]
                    prev_mod_children.append(modtype_dict)
                    mod_dict["children"] = prev_mod_children
                prev_subcat_children = subcat_dict["children"]
                prev_subcat_children.append(mod_dict)
                subcat_dict["children"] = prev_subcat_children
            prev_cat_children = cat_dict["children"]
            prev_cat_children.append(subcat_dict)
            cat_dict["children"] = prev_cat_children
        previous_children = response["children"]
        previous_children.append(cat_dict)
        response["children"] = previous_children
    return Response(json.dumps(response), mimetype='application/json')

# upload file to the repo
@app.route('/api/store/upload', methods=['GET', 'POST'])
def upload():
    if request.method != 'POST':
        return redirect('https://documeta.cs.odu.edu/files')

    if 'files' not in request.files:
        return jsonify({'success':False, 'reason':'No files part in request'}), 400

    uploads = request.files.getlist('files')
    for file in uploads:
        if file.filename == '': continue
        if file.filename.lower().endswith(backend.docutils.EXT):
            filename = backend.docutils.normalize(secure_filename(file.filename))
            file.save(os.path.join(os.environ['REPO'], filename))
        else: app.logger.warning('File "%s" uses an unsupported file format', file.filename)
    return jsonify(success=True), 200

# delete file from repo
@app.route('/api/store/delete/<string:fname>', methods=['DELETE'])
def delete(fname):
    doc = Document.query.filter_by(file_name=fname).first()
    if doc is not None:
        os.remove(doc.file_path)
        return jsonify(success=True)
    return jsonify({'success':False,'reason':'document does not exist'}), 400
