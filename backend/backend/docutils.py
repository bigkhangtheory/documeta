#!/usr/bin/env python3

import textract
import os, pickle, argparse, yaml, json

from multiprocessing import Pool

CHAR_MAX = 45

WORKSPACE = os.environ['DTMP'] + '/docutils/'

ADDED = WORKSPACE + 'added'
REMOVED = WORKSPACE + 'removed'

EXT = (".pdf", ".txt", ".docx", ".doc", ".pptx", ".html",
        ".csv", ".eml", ".epub", ".json", ".odt", ".pptx", ".ps",
        ".rtf", ".tiff", ".xlsx", ".xls")

def normalize(filename):
    filename.replace(' ', '_')
    name, ext = os.path.splitext(filename)
    name_len = len(name)
    if name_len > CHAR_MAX:
        filename = name[:-(name_len - CHAR_MAX)] + ext
    return filename

class last_occurrence(object):
    def __init__(self, pattern, chars):
        self.occurrences = {}
        for char in chars:
            self.occurrences[char] = pattern.rfind(char)

    def __call__(self, char):
        return self.occurrences[char]

def found_pattern(pattern, text):
    last = last_occurrence(pattern, set(text))
    m = len(pattern)
    n = len(text)
    i = m - 1
    j = m - 1
    while i < n:
        if text[i] == pattern[j]:
            if j == 0:
                return True
            else:
                i -= 1
                j -= 1
        else:
            l = last(text[i])
            i = i + m - min(j, 1+l)
            j = m - 1 
    return False

def writedocs(path, added):
    if os.path.exists(path) and os.stat(path).st_size > 0:
        with open(path, 'rb') as rfp: 
            data = pickle.load(rfp)
        data.extend(added)
        with open(path, 'wb') as wfp:
            pickle.dump(data, wfp)
    else:
        with open(path, 'wb') as fp:
            pickle.dump(added, fp)

def readdocs(path, removed=False):
    with open(path, 'rb') as fp: 
        data = pickle.load(fp)
    docs = []
    for f in data:
        if ((not removed and os.path.exists(f) and f.endswith(EXT))
                or (removed and f.endswith(EXT))): docs.append(f)
    return docs

def process_text(doc):
    try:
        return (doc, textract.process(doc).decode('utf-8').lower())
    except: return (doc, '')

def process():
    added = []
    if os.path.exists(ADDED) and os.stat(ADDED).st_size > 0:
        pool = Pool()
        docs = readdocs(ADDED)
        added = pool.map(process_text, docs)
        open(ADDED, 'w').close()
    return added

def clean():
    removed = []
    if os.path.exists(REMOVED) and os.stat(REMOVED).st_size > 0:
        removed = readdocs(REMOVED, removed=True)
        open(REMOVED, 'w').close()
    return removed

if __name__ == '__main__':
    if not os.path.exists(WORKSPACE):
        os.makedirs(WORKSPACE)

    parser = argparse.ArgumentParser()

    parser.add_argument('-a', '--add'   , nargs='+', default=[])
    parser.add_argument('-r', '--remove', nargs='+', default=[])

    args = parser.parse_args()

    if args.add:
        writedocs(ADDED, args.add)
    elif args.remove:
        writedocs(REMOVED, args.remove)
