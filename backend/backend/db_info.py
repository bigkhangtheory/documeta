from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from datetime import datetime
from backend import app

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)

class Document(db.Model):
    __tablename__ = 'Document'
    id = db.Column(db.Integer, primary_key=True)
    file_name = db.Column(db.String(50), unique=True)
    file_path = db.Column(db.String(150), unique=True)
    date_added = db.Column(db.DateTime)

    def __init__(self, file_name, file_path):
        self.file_name = file_name
        self.file_path = file_path
        self.date_added = datetime.utcnow()

class Category(db.Model):
    __tablename__ = 'Category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __init__(self, name):
        self.name = name

class SubCategory(db.Model):
    __tablename__ = 'SubCategory'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __init__(self, name):
        self.name = name

class Model(db.Model):
    __tablename__ = 'Model'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __init__(self, name):
        self.name = name

class ModelType(db.Model):
    __tablename__ = 'ModelType'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __init__(self, name):
        self.name = name

class Frequency(db.Model):
    __tablename__ = 'Frequency'
    id = db.Column(db.Integer, primary_key=True)
    doc_id = db.Column(db.Integer, db.ForeignKey('Document.id'))
    cat_id = db.Column(db.Integer, db.ForeignKey('Category.id'))
    subcat_id = db.Column(db.Integer, db.ForeignKey('SubCategory.id'))
    mod_id = db.Column(db.Integer, db.ForeignKey('Model.id'))
    modtype_id = db.Column(db.Integer, db.ForeignKey('ModelType.id'))

    def __init__(self, doc_id, cat_id, subcat_id, mod_id, modtype_id):
        self.doc_id = doc_id
        self.cat_id = cat_id
        self.subcat_id = subcat_id
        self.mod_id = mod_id
        self.modtype_id = modtype_id

class OrderOfBattle(db.Model):
    __tablename__ = 'OrderOfBattle'
    id = db.Column(db.Integer, primary_key=True)
    cat_id = db.Column(db.Integer, db.ForeignKey('Category.id'))
    subcat_id = db.Column(db.Integer, db.ForeignKey('SubCategory.id'))
    mod_id = db.Column(db.Integer, db.ForeignKey('Model.id'))
    modtype_id = db.Column(db.Integer, db.ForeignKey('ModelType.id'))

    def __init__(self, cat_id, subcat_id, mod_id, modtype_id):
        self.cat_id = cat_id
        self.subcat_id = subcat_id
        self.mod_id = mod_id
        self.modtype_id = modtype_id

class DocumentSchema(ma.ModelSchema):
    class Meta:
        model = Document

class CategorySchema(ma.ModelSchema):
    class Meta:
        model = Category

class SubCategorySchema(ma.ModelSchema):
    class Meta:
        model = SubCategory

class ModelSchema(ma.ModelSchema):
    class Meta:
        model = Model

class ModelTypeSchema(ma.ModelSchema):
    class Meta:
        model = ModelType

documents_schema = DocumentSchema(many=True,strict=True)
categories_schema = CategorySchema(many=True,strict=True)
sub_categories_schema = SubCategorySchema(many=True,strict=True)
models_schema = ModelSchema(many=True,strict=True)
model_types_schema = ModelTypeSchema(many=True,strict=True)
