#!/bin/bash

tmpdir=$DTMP/recevent/
new=$tmpdir/new
old=$tmpdir/old

mkdir -p $tmpdir && touch $new $old || exit
find $REPO -type f -printf "%TT %p\n" > $new
if ! cmp -s $new $old; then
	added=$(fgrep -vf $old $new | cut -d' ' -f2-)
	removed=$(fgrep -vf $new $old | cut -d' ' -f2-)
	if [[ $added = $removed && ! -z $added ]]; then
		docutils.py -a ${added[*]}
	else
		[[ ! -z $added ]] && docutils.py -a ${added[*]}
		[[ ! -z $removed ]] && docutils.py -r ${removed[*]}
	fi
	cp -f $new $old
fi
