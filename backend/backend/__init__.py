#!/usr/bin/env python3

from flask import Flask
import backend.credentials

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = backend.credentials.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

from backend.api import process, clean, sunburst, frequencies, docs, upload, delete

if __name__ == '__main__':
    app.run(debug=True)
