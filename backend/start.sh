#!/usr/bin/env bash

source ./venv/bin/activate
flask db init
flask db migrate
flask db upgrade
./init_app.py
exec gunicorn --bind=0.0.0.0:5000 --timeout 3600 --log-level=warning --access-logfile - --error-logfile - backend:app
