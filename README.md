# documeta.

documeta is a portable, lightweight document insight and comprehension tool 
that aims to provide a simple, intuitive interface to quickly 
derive relationships between large sets of documents and effectively explore 
data within those documents.

Visit our [website](https://www.cs.odu.edu/~411crystal) for more information.

## Getting started

* Install the newest version of docker-ce

* Install the newest version of docker-compose

* Run 'docker-compose up -d' in the root directory

* To stop a running instance run 'docker-compose down' in the directory which contains docker-compose.yml

* To delete the data volumes run 'docker-compose down -v'

* To view logs run 'docker logs -f <container name>'

* To view currently running docker-compose containers run 'docker-compose ps', this will also show container names


## Authors
* **Khang Nguyen**
* **Andrew Davidson**
* **Sean Tosloskie**
* **Nick Cason**
* **Alex Tucker**

## Special thanks
* **Captain Mark "Forrest" Stoops**
