export const state = () => ({
  docs: []
});

export const actions = {
  /**
   * FETCH documents from backend
   * @param commit
   * @returns {Promise<void>}
   * @constructor
   */
  async FETCH({commit}) {
    await this.$axios.get('https://documeta.cs.odu.edu/api/data/docs')
      .then((response) => {
        console.log(response);
          commit('SET', response.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },

};

export const mutations = {
  SET(state, payload) {
    state.docs = payload;
  }
};

export const getters = {
  GET(state) {
    return state.docs;
  }
}
