export const state = () => ({
  uploads: [],
  init: false,
  loaded: false
});

export const actions = {
  /**
   * retrieve the files from the backend
   *
   * @param commit
   * @returns {Promise<void>}
   * @constructor
   */
  async FETCH({commit}) {

        let nil = [];
        commit('SET', nil)
  },

  async INIT({commit}, payload) {
    commit('ADD', payload);
    commit('SET_INIT');
  },

  /**
   * send documents
   * @param commit
   * @param payload
   * @returns {Promise<void>}
   * @constructor
   */
  async POST({commit}, payload) {
    await this.$axios.$post('https://documeta.cs.odu.edu/api/store/upload', payload,
                      {
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                      })
        .then(function(response) {
          console.log(response);
          commit("SET_LOADED");
        })
        .catch(function(error) {
          console.log(error)
        });
  }
};

export const mutations = {
  /**
   * set the documents
   * @param state
   * @param payload
   * @constructor
   */
   SET(state, payload) {
    state.uploads = payload;
  },
  /**
   * add to the docs
   * @param state
   * @param payload
   * @constructor
   */
  ADD(state, payload) {
    state.uploads.push(payload);
  },

  SET_INIT(state) {
    state.init = true;
  },

  SET_LOADED(state) {
    state.loaded = true;
  }
};

export const getters = {
  /**
   * DOCS OBJECT
   * @param state
   * @returns {*}
   * @constructor
   */
  /*GET(state) {
    return state.uploads;
  },*/
};
