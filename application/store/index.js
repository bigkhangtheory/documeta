export const state = () => ({
  drawer: true
});
export const mutations = {
  toggleDrawer(state) {
    state.drawer = !state.drawer;
  },
  drawer(state, val) {
    state.drawer = val;
  }
};

export const actions = {
  async FLARE({dispatch}) {
    await dispatch('flare/FETCH');
  },
  async BRANCHES({dispatch}) {
    await dispatch('branches/FETCH');
  },
  async UPLOADS({dispatch}) {
   await dispatch('uploads/FETCH');
  },
  async TEST({dispatch}) {
    await dispatch('test/FETCH')
  },
  async DOCUMENTS({dispatch}) {
    await dispatch('documents/FETCH');
  },
  async KEYWORDS({dispatch}) {
    await dispatch('keywords/FETCH');
  },
  async PROCESS({dispatch}) {
    await dispatch('flare/PROCESS');
  }
};
