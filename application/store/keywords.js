export const state = () => ({
  keys: []
});

export const actions = {
  async FETCH({commit}) {
    await this.$axios.get('https://backend:5000/api/data/keys',
                          {process: true})
              .then((response) => {
                if(response.status === 200) {
                  commit('SET_KEYS', response.data)
                }
              })
              .catch((error) => {
                console.log(error)
              })
  }
};

export const mutations = {
  SET(state, params) {
    state.keys = params;
  }
};

export const getters = {
  GET(state) {
    return state.keys;
  }
};
