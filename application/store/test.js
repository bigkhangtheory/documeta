export const state = () => ({
  data: {}
});

export const actions = {
  async FETCH({ commit }) {
    await this.$axios.get('https://gist.githubusercontent.com/mbostock/1093025/raw/b40b9fc5b53b40836ead8aa4b4a17d948b491126/flare.json',
                           {progress: true})
      .then((response) => {
        if(response.status === 200) {
          commit( 'SET', response.data );
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }
};

export const mutations = {
  SET(state, params) {
    state.data = params;
  }
};

export const getters = {
  GET(state) {
    return state.data;
  }
};
