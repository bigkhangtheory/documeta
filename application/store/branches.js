
export const state = () => ({
  branches: [],
  count: 0
});


export const actions = {
  /**
   * retrieve branches
   * @param commit
   * @returns {Promise<void>}
   * @constructor
   */
  async FETCH({commit}) {
    await this.$axios.get('https://git-community.cs.odu.edu/api/v4/projects/931/repository/branches?per_page=100',
                          {headers: {
                            "Content-Type": "application/json",
                            "PRIVATE-TOKEN": "pjtYswHs2SzBcDkvZYXZ"
                            }})
      .then((response) => {
        if(response.status === 200) {
          commit('SET_BRANCHES', response.data);
          commit('SET_BRANCH_COUNT', response.data)

        }
      })
      .catch((error) =>
        console.log(error)
      )
  }
};

export const mutations = {
  /**
   * set branches
   * @param state
   * @param params
   * @constructor
   */
  SET_BRANCHES(state, params) {
    state.branches = params;
  },
  /**
   * set branch count
   * @param state
   * @param payload
   * @constructor
   */
  SET_BRANCH_COUNT(state, payload) {
    state.count = payload.length;
  }
};

export const getters = {
  BRANCHES(state) {
    return state.branches;
  },
  BRANCH_COUNT(state) {
    return state.count;
  }
};
