export const state = () => ({
  vis: null,
  loading: false
});

export const actions = {
  async FETCH({ commit }) {
    await commit('RESET');
    commit('LOADING');
    await this.$axios.$get('https://documeta.cs.odu.edu/api/process')
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });

    await this.$axios.get('https://documeta.cs.odu.edu/api/data/flare')
      .then((response) => {
          commit( 'SET', response.data );
          commit( 'LOADING');
      })
      .catch((error) => {
        console.log(error)
      })
  },

  async TEST({ commit }) {
    await commit('RESET');
    await this.$axios.get('https://gist.githubusercontent.com/mbostock/1093025/raw/b40b9fc5b53b40836ead8aa4b4a17d948b491126/flare.json')
              .then((response) => {
                commit( 'SET', response.data );
              })
              .catch((error) => {
                console.log(error)
              })
  },
};

export const mutations = {
  SET(state, params) {
    state.vis = params;
  },
  LOADING(state) {
    state.loading = !state.loading;
  },
  RESET: () => {}

};

export const getters = {
  GET(state) {
    return state.vis;
  }
};
