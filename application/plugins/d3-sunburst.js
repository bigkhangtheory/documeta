import Vue from 'vue'
import SunburstChart from '../components/widgets/sunburst/SunburstChart'
import highlightOnHover from "~/components/widgets/sunburst/highlightOnHover";
import zoomOnClick from "~/components/widgets/sunburst/zoomOnClick";

Vue.use(SunburstChart);
export {
  highlightOnHover,
  SunburstChart,
  zoomOnClick
};
