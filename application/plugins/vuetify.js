import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import { Ripple } from 'vuetify/lib/directives'

Vue.use( Vuetify, {
  customProperties: true,
  directives: {
    Ripple
  }
} );
