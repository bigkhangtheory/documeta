import Vue from 'vue'
import breadcrumbTrail from '~/components/widgets/sunburst/breadcrumbTrail'
Vue.use(breadcrumbTrail);
export {
  breadcrumbTrail
}
