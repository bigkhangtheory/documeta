import Vue from 'vue'
import breadcrumb from '~/components/widgets/sunburst/breadcrumb'
Vue.use(breadcrumb);
export {
  breadcrumb
}
