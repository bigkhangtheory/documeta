const Menu = [
  { header: 'Home'},
  {
    title: 'Welcome',
    group: 'home',
    name: 'welcome',
    icon: 'fa-home',
    href: '/welcome'
  },
  {
    title: 'Team',
    group: 'home',
    name: 'team',
    icon: 'fa-info-circle',
    href: '/team'
  },
  { divider: true },
  { header: 'Application' },
  {
    title: 'Dashboard',
    group: 'apps',
    icon: 'dashboard',
    name: 'Dashboard',
    href: '/dashboard'
  },
  {
    title: 'Files',
    group: 'apps',
    name: 'files',
    icon: 'perm_media',
    href: '/files'
  },
  { divided: true },

];
// reorder menu
Menu.forEach( (item) => {
  if( item.items ) {
    item.items.sort( (x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    } );
  }
} );
export default Menu;
