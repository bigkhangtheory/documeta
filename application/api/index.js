





import * as Post from './post';
import * as Chart from './chart';



export default {
 // project

 getPost: Post.getPost,
 // chart data
  getMonthVisit: Chart.monthVisitData,
  getCampaign: Chart.campaignData,
  getLocation: Chart.locationData,
};
