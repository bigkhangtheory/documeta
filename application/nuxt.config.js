const pkg = require( './package' );
const VuetifyLoaderPlugin = require( 'vuetify-loader/lib/plugin' );
module.exports = {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Documeta Frontend Dashboard',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description', name: 'description', content: 'Documeta Dashboard is a \n' +
          '    Google Material Design inspired document insight & comprehension tool built with Vue and Vuetify.'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/docker-darken.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons|Montserrat'
      }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.4/echarts-en.min.js' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#26a69a' },
  /*
   ** Global CSS
   */
  css: [
    '~/assets/style/theme.styl',
    '~/assets/style/app.styl',
    'font-awesome/css/font-awesome.css',
    'roboto-fontface/css/roboto/roboto-fontface.css',
    'typeface-montserrat/index.css',
    'vue-d3-sunburst/dist/vue-d3-sunburst.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/vee-validate',
    '~/plugins/vue-d3-sunburst',
    '~/plugins/vuetify',
    '~/plugins/vuetify-upload-button',
    '~/plugins/d3',
    '~/plugins/vue-async-computed',
    '~/plugins/d3-sunburst',
    '~/plugins/breadcrumb-trail',
    '~/plugins/breadcrumb',
    '~/plugins/vuex-reset'

  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    ['vue-wait/nuxt', { useVuex: true }],
    'nuxt-webfontloader',
    ['@nuxtjs/vuetify', {
      treeshake: true
    }]
  ],
  axios: {
    https: true,
    credentials: true
  },
  /*
   ** Properties of page transitions
   */
  transition: {
    name: 'page',
    mode: 'out-in',
    beforeEnter(el) {
      console.log( 'Before enter...' );
    }
  },
  /*
   ** Properties of layout transitions
   */
  layoutTransition: {
    name: 'layout',
    mode: 'out-in'
  },
  /*
   ** Build configuration
   */
  build: {

    plugins: [ new VuetifyLoaderPlugin() ],
    loaders: {
      stylus: {
        import: [ '~assets/style/variables.styl' ]
      },
   },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
    }
  }
};
